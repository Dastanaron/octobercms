#!/usr/bin/env bash

docker cp ./dev_dump.sql villages_pgsql_1:/
docker exec villages_pgsql_1 /bin/bash -c "psql -U pguser common < /dev_dump.sql"
