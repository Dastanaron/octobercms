#!/usr/bin/env bash

#Create dump in container
docker exec villages_pgsql_1 /bin/bash -c "pg_dump --clean -U pguser common > /database.sql"
docker cp villages_pgsql_1:/database.sql dev_dump.sql
